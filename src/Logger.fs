module ImageCombinator.Logger

open System
open System.IO
open System.Collections.Generic

open Serilog
open Serilog.Events

open ImageCombinator.Constant

type Logger () =
  let time = DateTime.Now.ToString("yyyy-MM-ddTHH_mm_ss")
  let logFile = Path.Join(LogDirectory, $"log-{time}.txt")
  let serilog = LoggerConfiguration()
                  .MinimumLevel.Debug()
                  .WriteTo.File(logFile, rollingInterval=RollingInterval.Infinite, shared=true)
                  .WriteTo.Console(restrictedToMinimumLevel=LogEventLevel.Information)
                  .CreateLogger()

  member _.Debug(message: string) = serilog.Debug(message)
  member _.Info(message: string) = serilog.Information(message)
  member _.Warning(message: string) = serilog.Warning(message)
  member _.Error(message: string) = serilog.Error(message)

let logger = Logger()
