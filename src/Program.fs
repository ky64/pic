﻿open ImageCombinator.Config
open ImageCombinator.Constant
open ImageCombinator.Metadata
open ImageCombinator.Util
open ImageCombinator.Logger

open System.IO

Directory.CreateDirectory(LogDirectory) |> ignore
let config =
  match loadConfig ConfigFile with
  | Ok data -> data |> checkConfig
  | Error _ -> defaultConfiguration // This will never returned since the program will close
                                    // when error occurs

let amount = Option.get config.amount
let batchSize = Option.get config.batchSize
let imageName = Option.get config.name
let checkpointFilename = createCheckpointFilename imageName
let startTokenId = Option.get config.startTokenId
let distribution = calculateDistribution config.distribution (config.amount|>Option.get)
let height = Option.get config.height
let width = Option.get config.width
let outputDirectory = Option.get config.outputDirectory
let outputPath = Path.Join(outputDirectory, imageName)
let generatedTokenIds = listGeneratedImages imageName
let mutable tokenIds = [startTokenId..(startTokenId+amount)-1u]

/// Helper function to insert traits combination into a list
let addTraits (traitList: List<Option<string seq>>) _ : List<Option<string seq>> =
  let traits =
    combineTraits config.layers (distribution|>Option.get)
  match traits with
  | Some _ -> traits :: traitList
  | None ->
    forceClose("")
    traitList

let traitsCollection =
  match shouldLoadCheckpointFile checkpointFilename
  with
    | true ->
            tokenIds <- tokenIds
                        |> List.except generatedTokenIds
            match List.isEmpty tokenIds with
            | true -> []
            | false ->
              generatedTokenIds
              |> loadCheckpointFile checkpointFilename
    | false ->
            let collection = generateTraits tokenIds addTraits
            deleteExistingFile checkpointFilename |> ignore
            collection |> createCheckpointFile imageName tokenIds

            collection

Directory.CreateDirectory(Path.Join(outputPath, ImageDirectory)) |> ignore
Directory.CreateDirectory(Path.Join(outputPath, MetadataDirectory)) |> ignore

let computations: seq<Async<unit>> =
  traitsCollection
  |> List.mapi (fun index traits ->
      async {
        let tokenId = tokenIds |> List.item index
        let metadataFilename = $"{tokenId}.json"
        let metadataPath = Path.Join(outputPath, MetadataDirectory, metadataFilename)
        let imageFilename = $"{tokenId}.png"
        let imagePath = Path.Join(outputPath, ImageDirectory, imageFilename)

        match shouldReplaceImage(imagePath)
        with
        | true ->
          let metadata = createMetadata(imageName, int tokenId, None, imagePath, traits, None)
          let metadataJson = toJsonString metadata
          logger.Info($"Save metadata to {metadataPath}")
          File.WriteAllText(metadataPath, metadataJson)
          logger.Info($"[{index+1}/{amount}] generating image {imagePath}")
          generateImage imagePath (width, height) (Option.get traits)
        | false -> ()
      })
  |> Seq.ofList

let taskResult =
  Async.Parallel(computations, maxDegreeOfParallelism=int batchSize)
  |> Async.StartAsTask
taskResult.Wait()

checkpointFilename
|> deleteExistingFile
