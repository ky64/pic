namespace ImageCombinator

open System;
open System.IO;
open System.Text.Json;

open SixLabors.ImageSharp;
open SixLabors.ImageSharp.Processing;

open ImageCombinator.Constant;
open ImageCombinator.Logger;

/// Schema for asset collection
/// Key: Layer name
/// Value: Array of path to asset
type AssetCollection = Map<string, string[]>

/// <summary> Helper type to return same value multiple times
/// It's a replica of <seealso href="https://docs.python.org/3/library/itertools.html#itertools.repeat">itertools.repeat</seealso> from Python </summary>
/// <param name="value"> Value that will be returned repeatedly </param>
/// <param name="count"> How many times it should return the value </param>
/// <returns> Value of <paramref name="value" /> parameter </returns>
type AssetDistribution(value: string, count: uint) =
  let mutable repeat: uint = count

  /// Return <paramref name="value"> and reduce the quota by 1
  /// If the quota already 0 it will return an Error
  member _.Next =
    match repeat with
    | number when number = 0u -> 
      logger.Debug($"{value} has {repeat} quota. Returning 'Out of quota'")
      Error ("Out of quota")
    | _ ->
      logger.Debug($"{value} has quota {repeat}, reducing it to {repeat-1u}")
      repeat <- repeat - 1u
      Ok (value)

  /// How many quota left
  member _.Quota = 
    logger.Debug($"{value} has {repeat} quota.")
    repeat

/// Schema of 'distribution' key in config file
type ConfigDistribution = Map<string, Map<string, float>>

/// Distribution of the asset
type Distribution = Map<string, Map<string, AssetDistribution>>

/// Schema of JSON configuration file
type ConfigObject = {
  /// Name of the image
  name: Option<string>
  /// Amount of image to generate
  amount: Option<uint>
  /// Size of batch for processing the image
  batchSize: Option<byte>
  /// First token ID of generated image and metadata
  startTokenId: Option<uint>
  /// Image width
  width: Option<uint16>
  /// Image height
  height: Option<uint16>
  /// Output directory
  outputDirectory: Option<string>
  /// Whether generating identical image is allowed
  unique: Option<bool>
  /// Collection of folder name that contains images to be used as asset.
  /// The first index will be placed at the bottom, then the next one goes above it
  layers: Option<string list>
  /// Distribution of the image trait
  /// i.e. 5 out of 10 images has Red background
  distribution: Option<ConfigDistribution>
}


module Util =
  /// <summary> Print message then stop process with exit code 1 </summary>
  /// <params name="message"> Message that will be printed before closing the program </params>
  let forceClose (message: string): unit =
    match String.IsNullOrEmpty message with
    | false ->
      logger.Error($"Script is force stopped with message: {message}")
      eprintfn "%s\n" message
    | true -> ()
    printfn "\nPress any button to exit!"
    System.Console.ReadKey(intercept=true) |> ignore
    System.Environment.Exit(1)

  /// <summary> Helper function to combine image </summary>
  /// <param name="baseImage"> The initial image that will be combined with other images </param>
  /// <param name="otherImage"> Image that will be combined with <paramref name="baseImage"/> </param>
  let private processImage (baseImage: Image, otherImage: Image): unit =
    baseImage.Mutate(fun action -> action.DrawImage(otherImage, 1f) |> ignore)
    otherImage.Dispose()

  /// <summary> Wrapper function to load image with ImageSharp. It will force close
  /// if the image has invalid format </summary>
  /// <param name="imagePath"> Path to image </param>
  /// <returns> Image data </returns>
  let loadImage (imagePath: string): Option<Image> =
    try
      logger.Debug($"Load image {imagePath}")
      imagePath |> Image.Load |> Some
    with
    | :? UnknownImageFormatException as err ->
      forceClose($"Image '{imagePath}' has invalid format: {err.Message}")
      None

  /// <summary> Combine multiple images into one frame </summary>
  /// <param name="images"> Collection of image path </param>
  /// <param name="imageWidth"> Image width </param>
  /// <param name="imageHeight"> Image height </param>
  /// <param name="output"> Path to store the output image </param>
  let generateImage (output: string) (imageWidth: uint16, imageHeight: uint16) (images: seq<string>) =
    let width = int imageWidth
    let height = int imageHeight
    use baseImage: Image =
      images
      |> Seq.head
      |> loadImage
      |> Option.get

    try
      logger.Debug($"Resize background to {imageWidth}x{imageHeight}")
      baseImage.Mutate(fun img -> img.Resize(width, height) |> ignore)
    with
    | :? UnknownImageFormatException as err -> forceClose($"Unknown image {Seq.head images} format: {err.Message}")
    images
    |> Seq.skip 1
    |> Seq.map (fun img ->
      let otherImage: Image = loadImage(img) |> Option.get
      logger.Debug($"Resize {img} to {imageWidth}x{imageHeight}")
      otherImage.Mutate(fun img -> img.Resize(width, height) |> ignore)
      (baseImage, otherImage)
    )
    |> Seq.iter processImage
    logger.Info($"Save image to {output}")
    baseImage.Save(output)

  /// <summary> Helper function to match file extension </summary>
  /// <param name="filename"> Path to file </param>
  /// <param name="extension"> Expected file extension </param>
  /// <returns> 'true' if extension matches otherwise 'false' </returns>
  let matchExtension (filename: string, extension: string): bool =
    match File.Exists(filename) with
    | true -> 
      let isMatch: bool = FileInfo(filename).Extension = extension
      logger.Debug($"{filename} has extension {extension}? {isMatch}")
      isMatch
    | false -> 
      logger.Warning($"Can not found {filename} to match its extension")
      false

  /// <summary> Load all images in <see cref="P:Program.LayerDirectory"> layers directory </see> </summary>
  /// <param name="directories"> A list of directory in layers directory </param>
  /// <returns> Map of layers directory </returns>
  let collectImages (directories: string list): Option<Map<string, string[]>> =
    try
      let listImages (imageDirectory: string) =
        logger.Info($"Collect images with {ImageFormat} format from {imageDirectory}")
        imageDirectory
        |> Directory.GetFiles
        |> Array.filter (fun file -> matchExtension(file, ImageFormat))
      directories
      |> List.map (fun dir -> Path.Combine(LayerDirectory, dir))
      |> List.map (fun dir -> (Path.GetFileName(dir), listImages(dir)))
      |> Map.ofList
      |> Some
    with
    | :? PathTooLongException as err ->
      logger.Debug($"File path is too long: {err}")
      forceClose($"Path is too long: {err.Message}")
      None
    | :? ArgumentException as err ->
      logger.Debug($"Invalid character in the file path: {err}")
      forceClose($"Path contains invalid character: {err.Message}")
      None
    | :? DirectoryNotFoundException as err ->
      forceClose($"Directory not found: {err.Message}")
      None
    | :? IOException as err ->
      logger.Debug($"Fail to collect images due to IOException: {err}")
      forceClose($"File type error: {err.Message}")
      None
    | :? UnauthorizedAccessException as err ->
      logger.Debug($"Fail to collect images due to UnauthorizedAccessException: {err}")
      forceClose($"Don't have permission to read directory: {err.Message}")
      None

  /// <summary> Convert record to formatted JSON string
  /// If there is any null value, it will be discarded from the JSON </summary>
  /// <param name="input"> Any record that's convertible to JSON </param>
  /// <returns> Formatted JSON string </returns>
  let toJsonString (input): string =
    let option = JsonSerializerOptions(WriteIndented=true,
                                       DefaultIgnoreCondition=Serialization.JsonIgnoreCondition.WhenWritingNull)
    logger.Debug($"Convert JSON object to string:\n{input}")
    JsonSerializer.Serialize(input, option)

  /// <summary> Helper function to create checkpoint filename </summary>
  /// <param name="name"> Name of image in config.json </param>
  /// <returns> Checkpoint filename </returns>
  let createCheckpointFilename (name: string) =
    $"{name}-traits-checkpoint.csv"

  /// <summary> Create path where generated image is stored </summary>
  /// <param name="name"> 'name' value in config.json </param>
  /// <returns> Path to image directory </returns>
  let createImageDirectoryPath (name: string) =
    Path.Join(OutputDirectory, name, ImageDirectory)

  /// <summary> Create path where generated metadata is stored </summary>
  /// <param name="name"> 'name' value in config.json </param>
  /// <returns> Path to metadata directory </returns>
  let createMetadataDirectoryPath (name: string) =
    Path.Join(OutputDirectory, name, MetadataDirectory)

  /// <summary> Create a CSV file based on a list of data </summary>
  /// <param name="filename"> CSV filename to store the data </param>
  /// <param name="data"> Data that will be written to CSV </param>
  let createCsv (filename: string) (data: List<Option<string seq>>) =
    let write (content: Option<string seq>) =
      let traits = match content with
                    | Some value -> 
                      value |> String.concat ","
                    | None -> ""
      File.AppendAllText(filename, traits+"\n")

    logger.Info($"Create CSV {filename}")
    data |> List.iter write

  /// <summary> Helper function to load CSV file </summary>
  /// <param name="filename"> CSV file path </param>
  /// <returns> CSV content </returns>
  let loadCsv (filename: string): Collections.Generic.IEnumerable<string array> =
    match File.Exists filename with
    | true ->
      logger.Info($"Load CSV {filename}")
      File.ReadLines(filename)
      |> Seq.map (fun line -> line.Split(","))
    | false -> 
      logger.Warning($"{filename} does not exist! Returning empty data")
      Seq.empty

  /// <summary> Read CSV checkpoint file and load the data for generating image </summary>
  /// <param name="filename"> Checkpoint file path </param>
  /// <param name="excludeTokenIds"> List of token ID to be excluded </param>
  /// <returns> List of traits combination in the checkpoint file </returns>
  let loadCheckpointFile (filename: string) (excludeTokenIds: List<uint>): List<Option<string seq>> =
    let tokenIdColumnIndex = 1
    let excludeGenerated (data: string array): bool =
      let tokenId = uint data[0]
      excludeTokenIds
      |> Set.ofList
      |> Set.contains tokenId
      |> not

    let createTraitsCollection (data: string array): Option<string seq> =
      data
      |> Seq.ofArray
      |> Seq.skip tokenIdColumnIndex // Exclude token ID column
      |> Some

    logger.Info($"Load checkpoint file {filename}")
    filename
    |> loadCsv
    |> List.ofSeq
    |> List.filter excludeGenerated
    |> List.map createTraitsCollection

  /// <summary> Delete file if exists. This function is an alternative </summary>
  /// <param name="filename"> File path to download </param>
  let deleteExistingFile (filename :string) =
    match File.Exists filename with
    | true -> 
      logger.Info($"Remove file {filename}")
      File.Delete filename
    | false -> ()

  /// <summary> Delete folder containing metadata and generated images </summary>
  /// <param name="name"> 'name' value in config.json </param>
  let deleteGeneratedImages (name :string) =
    let imageDirectory = Path.Join(OutputDirectory, name)
    match Directory.Exists imageDirectory with
    | true -> 
      logger.Warning($"Removing {imageDirectory} directory")
      Directory.Delete(imageDirectory, recursive=true)
    | false -> ()

  /// <summary> Match prompt answer, "Y" or "YES" will return true
  /// otherwise it will return false </summary>
  /// <param name="answer"> User input </param>
  /// <returns> true for "Y" or "YES" else is false </returns>
  let evaluateAnswer (answer: string) =
    logger.Debug($"Input answer: {answer}")
    match answer with
    | value when value = "Y" || value = "YES" -> 
      logger.Debug($"Answer {answer} returns 'true'")
      true
    | _ -> 
      logger.Debug($"Answer {answer} returns 'false'")
      false

  /// <summary> Trigger user input when file exists </summary>
  /// <param name="filename"> Path to file </param>
  /// <param name="prompt"> Message prompt for user </param>
  /// <returns> Depends on user input, 'true' for "Y" or "YES" otherwise it's false.
  /// it will return false if file does not exist </returns>
  let fileExistsPrompt (filename: string, prompt: string): bool =
    match File.Exists(filename) with
    | true -> printfn "%s" prompt
              logger.Debug($"Trigger prompt for checking whether file {filename} exists. Prompt: {prompt}")
              Console.ReadLine().ToUpper() |> evaluateAnswer
    | false -> false

  let shouldReplaceImage (filename: string): bool =
    match File.Exists(filename) with
    | true -> printfn "Image '%s' exists! Do you want to replace it? [Y/N]" filename
              logger.Debug($"Trigger prompt for replacing file {filename}")
              Console.ReadLine().ToUpper() |> evaluateAnswer
    | false -> true

  /// <summary> List generated images and return its token ID </summary>
  /// <param name="name"> 'name' value in config.json </param>
  /// <returns> Returns a list of token ID of generate image </returns>
  let listGeneratedImages (name: string): List<uint> =
    let isNumber (path: string): bool =
      let (result, _) =
        path
        |> Path.GetFileNameWithoutExtension
        |> UInt32.TryParse
      result

    let imageDirectoryPath =
      name
      |> createImageDirectoryPath

    let generatedImages = 
      match Directory.Exists imageDirectoryPath with
      | true ->
              imageDirectoryPath
              |> Directory.EnumerateFiles
              |> Seq.filter (fun image -> image |> Path.GetFileNameWithoutExtension |> isNumber)
              |> Seq.map (fun image -> image |> Path.GetFileNameWithoutExtension |> uint)
              |> List.ofSeq
      | false -> []
    logger.Debug($"Generated images in {imageDirectoryPath}:\n{generatedImages}")
    generatedImages

  /// <summary> Trigger prompt to load check point file </summary>
  /// <param name="filename"> Path to checkpoint file </param>
  /// <returns> Depends on user input, 'true' for "Y" or "YES" otherwise it's false.
  /// it will return false if file does not exist </returns>
  let shouldLoadCheckpointFile (filename: string): bool =
    fileExistsPrompt (filename, $"File {filename} exist! Do you want to continue generate image using it? [Y]es/[N]o")

  /// <summary> Check whether file exists and ask user prompt to overwrite file.
  /// If the file does not exists it will return 'true' </summary>
  /// <param name="filename"> File path to overwrite </param>
  /// <returns> 'true' means overwrite, otherwise 'false' </returns>
  let shouldOverwriteFile (filename: string): bool =
    fileExistsPrompt (filename, $"File {filename} exist! Do you want to overwrite it? [Y]es/[N]o")

  /// <summary> Create CSV that contains list of traits combination that will be generated as image.
  /// It's useful to keep track last generated image in case the script stopped due to some error </summary>
  /// <param name="name"> value of 'name' in config.json </param>
  /// <param name="tokenIds"> List of token IDs </param>
  /// <param name="data"> List of traits combination </param>
  let createCheckpointFile (name: string) (tokenIds: List<uint>) (data: List<Option<string seq>>) =
    let filename: string = createCheckpointFilename(name)
    let includeTokenId (traits: Option<string seq>) (tokenId: uint) =
      match traits with
      | Some traits ->
        traits |> Seq.append [string tokenId] |> Some
      | None -> None
    logger.Info($"Create checkpoint file {filename}")
    (data, tokenIds)
    ||> List.map2 includeTokenId
    |> createCsv filename


  /// <summary> Calculate distribution for each asset. It uses formula (asset distribution / 100) x totalAmount </summary>
  /// <param name="distribution"> The distribution from configuration file (config.json) </param>
  /// <param name="totalAmount"> The amount of image to be generated </param>
  /// <returns> Updated distribution of each asset </returns>
  let calculateDistribution (distribution: Option<ConfigDistribution>) (totalAmount: uint): Option<Distribution> =
    let calculate (key: string) (value: float) =
      let newDistribution = ((value/100.0) * float totalAmount) |> max 0.0 |> Math.Ceiling
      logger.Debug($"{key} distribution: {newDistribution}")
      AssetDistribution(key, uint newDistribution)

    let processing _ (asset: Map<string, float>) = // Skip 'key' parameter
      asset
      |> Map.map calculate

    match distribution with
    | Some value -> value |> Map.map processing |> Some
    | None -> None

  /// <summary> Pick asset randomly in array </summary>
  /// <param name="assets"> Array of path to asset </param>
  /// <returns> Path to asset in string </returns>
  let pickRandomAsset (assets: AssetDistribution[]): string =
    let random = System.Random()
    let getValue (selected: AssetDistribution): string =
      match selected.Next with
      | Ok value -> 
        logger.Debug($"Pick {selected} from {assets}")
        value
      | Error _ -> ""

    match Array.isEmpty assets with
    | true -> ""
    | false -> assets
              |> Array.length
              |> random.Next
              |> Array.get assets
              |> getValue

  /// <summary> Generate traits combination </summary>
  /// <param name="layers"> List of layer </param>
  /// <param name="distribution"> Map of asset distribution </param>
  /// <returns> List of assets </returns>
  let combineTraits (layers: Option<string list>) (distribution: Distribution): Option<string seq> =
    let listAvailableAsset (assets: seq<AssetDistribution>) =
      assets
      |> Seq.filter (fun asset -> asset.Quota > 0u)
      |> Seq.toArray

    let combine (layer: string): string =
      let assets =
        distribution[layer].Values
        |> listAvailableAsset

      logger.Debug($"{Array.length assets} asset available for layer {layer}")

      match Array.isEmpty assets with
      | true ->
        printfn "Out of quota! Layer %s is empty. Try increase the asset distribution" layer
        ""
      | false ->
        assets
        |> pickRandomAsset


    let traitsCollection = layers
                           |> Option.get
                           |> List.map combine

    match traitsCollection |> List.tryFind String.IsNullOrEmpty
    with
    | Some _ -> None
    | None -> traitsCollection |> Seq.ofList |> Some

  /// <summary> Generate a collection of traits combination </summary>
  /// <param name="tokenIds"> List of token IDs </param>
  /// <param name="addTraits"> Function that iterates sequence of traits and return a list of traits combination</param>
  /// <returns> A list of traits combination </returns>
  let generateTraits (tokenIds: List<uint>) (addTraits: List<Option<string seq>> -> _ -> List<Option< string seq>>) =
    ([], tokenIds)
    ||> List.fold addTraits


module Config =
  let defaultConfiguration: ConfigObject = {
    name = Some DefaultName;
    amount = Some DefaultAmount;
    batchSize = Some DefaultBatchSize;
    startTokenId = Some DefaultStartTokenId;
    width = Some DefaultWidth;
    height = Some DefaultHeight;
    outputDirectory = Some OutputDirectory;
    unique = Some DefaultUnique;
    layers = Some [];
    distribution = None;
  }

  /// <summary> Overwrite JSON configuration file and update distribution value </summary>
  /// <param name="config"> New configuration </param>
  let updateDistributionConfig (config: ConfigObject): unit =
    let distributionMap =
      config.layers
      |> Option.map Util.collectImages
      |> Option.map (fun imagePaths ->
        Option.get imagePaths
        |> Map.map( fun _ value ->
          value
          |> Array.map(fun filename -> (filename, 0.0))
        |> Map.ofArray)
       )

    let option = JsonSerializerOptions(WriteIndented=true)

    let jsonstring =
      match config with
      | { distribution = value } when value = None
        || value |> Option.get |> Map.isEmpty = true ->
        logger.Warning("Can not find value of 'distribution'")
        JsonSerializer.Serialize({ config with distribution = distributionMap }, option)
      | _ -> JsonSerializer.Serialize(config, option)

    try
      logger.Debug($"Overwrite {ConfigFile}:\n{jsonstring}")
      File.WriteAllText (ConfigFile, jsonstring)
    with
    | :? IOException as err -> 
      logger.Debug($"Fail to update configuration: {err}")
      Util.forceClose($"Error when updating configuration file: {err.Message}")
    | :? System.Security.SecurityException as err ->
      logger.Debug($"Fail to update configuration: {err}")
      Util.forceClose($"Got permission error when updating configuration file: {err.Message}")
    | :? UnauthorizedAccessException as err -> 
      logger.Debug($"Fail to update configuration: {err}")
      Util.forceClose($"Can't override configuration. File {ConfigFile} is read-only")

  /// <summary> Check configuarion value, if any 'null' value exist it will overwrite it with
  /// default value. However, null on 'layers' and 'distribution' value will cause the program
  /// stop execution since it requires user manually add and update the configuration
  /// </summary>
  /// <param name="config"> Parsed configuration from config.json </param>
  /// <returns> JSON Object </returns>
  let checkConfig (config: ConfigObject): ConfigObject =
    logger.Debug($"Parsed configuration:\n{config}")
    let updateConfig = { config with
                          name = (DefaultName, config.name) ||> Option.defaultValue |> Some
                          amount = (DefaultAmount, config.amount) ||> Option.defaultValue |> Some
                          batchSize = (DefaultBatchSize, config.batchSize) ||> Option.defaultValue |> Some
                          startTokenId = (DefaultStartTokenId, config.startTokenId) ||> Option.defaultValue |> Some
                          width = (DefaultWidth, config.width) ||> Option.defaultValue |> Some
                          height = (DefaultHeight, config.height) ||> Option.defaultValue |> Some
                          outputDirectory = (OutputDirectory, config.outputDirectory) ||> Option.defaultValue |> Some
                          unique = (DefaultUnique, config.unique) ||> Option.defaultValue |> Some
    }

    logger.Debug($"Updated configuration:\n{updateConfig}")
    updateConfig
    |> updateDistributionConfig

    match config with
    | { layers = value } when value = None || value = Some [] -> Util.forceClose($"Layers key should not be empty. Please update in {ConfigFile}")
    | { distribution = value } when value = None
      || value |> Option.get |> Map.isEmpty = true ->
          Util.forceClose($"Distribution is empty. Please update 'distribution' in {ConfigFile}")
    | _ -> ()

    updateConfig

  /// <summary> Load configuration from <see cref="P:Program.ConfigFile"> JSON file </see> </summary>
  /// <param name="file"> Configuration file path </param>
  /// <returns> JSON object </returns>
  let loadConfig (file: string): Result<ConfigObject,unit> =
    try
      logger.Info($"Load configuration from {ConfigFile}")
      let content = file
                    |> File.ReadAllText
      logger.Debug($"{ConfigFile} content:\n{content}")
      let value = content
                  |> JsonSerializer.Deserialize
                  |> Ok
      value
    with
    | :? FileNotFoundException ->
      logger.Warning($"File {ConfigFile} is missing! creating one...")
      checkConfig defaultConfiguration |> ignore
      Util.forceClose("") |> Error
    | :? JsonException as err ->
      logger.Debug($"Failed to parse JSON in {ConfigFile}: {err}")
      Util.forceClose($"File {ConfigFile} contains malformed JSON: {err.Message}\nAdditional info: {err.InnerException.Message}") |> Error
    | :? System.Security.SecurityException as err ->
      logger.Debug($"Failed to read {ConfigFile} due to SecurityException: {err}")
      Util.forceClose($"Got permission error when loading configuration file: {err.Message}") |> Error
    | :? IOException as err ->
      logger.Debug($"Failed to read {ConfigFile} due to IOException: {err}")
      Util.forceClose($"Error occurs when loading configuration file: {err.Message}") |> Error
