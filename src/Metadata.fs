module ImageCombinator.Metadata

open System.IO

open ImageCombinator.Logger

/// Schema of attribute based on OpenSea metadata standard
type Attributes = {
  trait_type: string
  value: string
}

/// Schema of metadata based on OpenSea metadata standard
type Metadata = {
  name: string
  description: Option<string>
  image_url: string
  attributes: Option<Attributes list>
  external_url: Option<string>
}

/// <summary> Helper function to convert sequence of asset path into
/// a list of attribute based on Attributes schema </summary>
/// <param name="traits"> Sequence of asset path used by an image </param>
/// <returns> A list of attribute </returns>
let createAttributes (traits: Option<string seq>): Option<Attributes list> =
  let create (attributes: List<Attributes>) (assetPath: string): List<Attributes>=
    let directory = Path.GetDirectoryName(assetPath)
    let trait_type = Path.GetFileName(directory)
    let value = Path.GetFileNameWithoutExtension(assetPath)
    logger.Debug($"Create attribute from {assetPath}. [trait_type: {trait_type}] [value: {value}]")
    { trait_type = trait_type; value = value } :: attributes

  match traits with
  | Some value -> ([], value) ||> Seq.fold create |> List.sort |> Some
  | None -> None

/// <summary> Helper function to create metadata based on Metadata schema </summary>
/// <param name="name"> Image name </param>
/// <param name="tokenId"> Token ID information in the metadata </param>
/// <param name="description"> Description that will be put in the metadata </param>
/// <param name="image_url"> URL of the image when it's stored in the cloud storage </param>
/// <param name="traits"> Sequence of asset path that will be converted to attributes </param>
/// <param name="external_url"> value of external_url in the metadata </param>
/// <returns> Image metadata based on Metadata schema </returns>
let createMetadata (name: string, tokenId: int, description: Option<string>, image_url: string, traits: Option<string seq>, external_url: Option<string>): Metadata =
  let attributes =
    traits
    |> createAttributes

  { name = $"{name} #{tokenId}";
    description = description;
    image_url = image_url;
    attributes = attributes;
    external_url = external_url }
