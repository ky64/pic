module ImageCombinator.Constant

/// Supported image format
[<Literal>]
let ImageFormat = ".png"
/// JSON file for configuration
[<Literal>]
let ConfigFile = "config.json"
/// Directory to store asset for each layer
[<Literal>]
let LayerDirectory = "layers"
/// Directory to store metadata
[<Literal>]
let MetadataDirectory = "metadata"
/// Directory to store image
[<Literal>]
let ImageDirectory = "images"
/// Directory to store the generated image and metadata
[<Literal>]
let OutputDirectory = "output"
/// Directory to store log
[<Literal>]
let LogDirectory = "log"
/// Default image name
[<Literal>]
let DefaultName: string = "Gambar"
/// Default batch size
[<Literal>]
let DefaultBatchSize: byte = 4uy
/// Default log level
[<Literal>]
let DefaultLogLevel: string = "info"
/// Default first token ID of generated image and metadata
[<Literal>]
let DefaultStartTokenId: uint = 1u
/// Default amount of image to be generated
[<Literal>]
let DefaultAmount: uint = 10u
/// Default height of the image
[<Literal>]
let DefaultHeight: uint16 = 512us
/// Default width of the image
[<Literal>]
let DefaultWidth = DefaultHeight
/// Default value of unique settings
[<Literal>]
let DefaultUnique: bool = false

