#!/usr/bin/env sh
 
PROJECT_NAME=pic
VERSION=0.0.1

create_release() {
  PROJECT_NAME_PARAM=$1
  VERSION_PARAM=$2
  PLATFORM_FOLDER_PARAM=$3
  RELEASE_FILE="$PROJECT_NAME_PARAM-$VERSION_PARAM-$PLATFORM_FOLDER_PARAM.zip"
  cp -r src/bin/Release/net6.0/$PLATFORM_FOLDER_PARAM/publish/ $PROJECT_NAME_PARAM;
  cp -r src/config.json src/layers $PROJECT_NAME_PARAM;
  zip -r $RELEASE_FILE $PROJECT_NAME_PARAM
  rm -rf $PROJECT_NAME_PARAM;
}

dotnet clean -c Release;
rm -rf $PROJECT_NAME-$VERSION*
rm -rf docs/_build \
       docs/env \
       src/bin \
       src/output \
       src/log src/obj \
       tests/bin \
       tests/obj

tar --exclude=.git* --exclude=*.tar.* -cJvf $PROJECT_NAME-$VERSION-source.tar.xz .

dotnet publish --os linux -c Release --self-contained true src/;
dotnet publish --os win -c Release --self-contained true src/;
dotnet publish --os osx -c Release --self-contained true src/;

create_release $PROJECT_NAME $VERSION win-x64
create_release $PROJECT_NAME $VERSION linux-x64
create_release $PROJECT_NAME $VERSION osx-x64
