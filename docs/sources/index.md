# Welcome to {{project_plain}}'s documentation!

```{toctree}
:caption: "Tutorial:"
:hidden:

tutorial/getting-started
tutorial/metadata
tutorial/custom-image
```
```{toctree}
:caption: "Reference:"
:hidden:
:glob:

reference/*
```
```{toctree}
:hidden:

appendix
```

If you are looking for a tool to combine multiple PNG images into a 
single image, you have come to the right place! {{project}} is a PNG image 
combinator and it does more than that! It can also create a random combination
for you so it _may_ generate different image each time if we provide it with
more images.

```{warning}
This tool is still in **experimental** phase. It's not recommended for production
```

```{figure} _static/showcase.gif
:alt: The picture is showing {{project}} tool is combining multiple images then produce several different images

Combine image using {{project}}
```

To get the idea on what happens in the preview above, take a look at the simplified diagram below

```{tip}
Right-click on the image and click "Open image in new tab" to get a better 
view

If you're on mobile, press and hold the image and open in a new tab
```

![overview-workflow](./_static/overview-workflow.png)

As we can see from the diagram above, {{project}} will go to **layers** directory then list down image inside each 
directory. After that, it will pick **one** image for each directory then combine them into a single image.
If one of the directory has multiple images, it will create a different combination to produce different image.

Now let's try to use it to get a better idea on how it works! Go to **[](./tutorial/getting-started)**
