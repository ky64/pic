# Appendix


## Thanks!

{{project}} is made not without any support. It's initially inspired by an interesting tool named [HashLips](https://github.com/HashLips/hashlips_art_engine).
I made this as a toy project to experiment with a different programming language, [F#](http://fsharp.org/).

> Any flaw in this tool is not representing the flaw of the following list but a pure human side of the 
> author

- [HashLips](https://github.com/HashLips/hashlips_art_engine)
- [F#](https://fsharp.org/)
- [F# for Fun and Profit](https://fsharpforfunandprofit.com/)
- [Ian Russel](https://ijrussell.github.io/about/) for his great book [Essential F#](https://leanpub.com/essential-fsharp)
- [ImageSharp](https://sixlabors.com/products/imagesharp/)
- [Serilog](https://serilog.net/)
- [Matthias Geier](https://github.com/mgeier) for his clean [sphinx theme](https://insipid-sphinx-theme.readthedocs.io/en/0.4.1/)
- **You!** For checking out and trying {{project}} tool.

## Source Code

If you're interested to see the source code or wanting to modify it check out [here](repo:pic)
