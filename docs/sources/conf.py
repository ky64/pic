project = "pic"
copyright = "2023, KY64"
author = "KY64"
release = "0.0.1"
extensions = ["myst_parser"]
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", "env", "etc"]
html_theme = "insipid"
html_static_path = ["_static"]
html_use_index = False
myst_enable_extensions = [
    "attrs_block",
    "colon_fence",
    "substitution"
]
myst_url_schemes = {
    "http": None,
    "https": None,
    "repo": "https://codeberg.org/ky64/{{path}}",
    "release": "https://codeberg.org/ky64/{{path}}/releases",
}
myst_substitutions = {
    "project": f"`{project}`",
    "project_plain": project,
    "extract_linux_file": f"""
    unzip {project}-{release}-linux-x64.zip""",
}
