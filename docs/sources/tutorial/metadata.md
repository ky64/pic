# Metadata

Metadata is an information related to an image. This metadata is referring to [metadata standard](https://docs.opensea.io/docs/metadata-standards)
that is used in [OpenSea](https://opensea.io/). This feature is still half-baked so [HashLips](https://github.com/HashLips/hashlips_art_engine/) can do better than this.

> If you're not interested to publish your image in [NFT](https://ethereum.org/en/nft/#what-are-nfts) marketplace, ignore this entire page since metadata
> often used for that purpose.

When we generate image, {{project}} will also generate its metadata in the **metadata** directory. This 
directory can be found inside **output/{name}/metadata** directory. `{name}` here means the value of `name` in 
our **config.json**. By default, the `name` configuration is _Gambar_ so it means the metadata is in 
**output/Gambar/metadata**.

```{hint}
Do you know that _Gambar_ is Indonesia word for "Picture"?
```

Let's have a look inside the **metadata** directory and we'll find a lot of file with [JSON](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/JSON) format.

```{figure} ../_static/metadata-directory.png
:alt: There are 10 files that are named using number and each of them has ".json" suffix. For example, 1.json, 2.json, 3.json, and so on.

Metadata directory
```

Let's open **1.json**, you may get different content but we'll focus on the structure.

```json
{
  "name": "Gambar #1",
  "image_url": "output/Gambar/images/1.png",
  "attributes": [
    {
      "trait_type": "Eyes",
      "value": "Dark"
    },
    {
      "trait_type": "Head",
      "value": "Gray"
    },
    {
      "trait_type": "Head Accessories",
      "value": "Hairless"
    },
    {
      "trait_type": "Mouth",
      "value": "Basic"
    }
  ]
}
```

You should get similar structure but with different value. Let's cover them one by one.

## `name`

This part is the image name. This name is taken from `name` value in **config.json**. Each image will have the 
same name in the metadata but with different suffix. Image **1.png** metadata is in **1.json** and
its name in the metadata is _Gambar #**1**_, where number **1** represent the image filename. Check the diagram 
below to understand more.

```{figure} ../_static/metadata-image-mapping.png
:alt: hello
Image and metadata relation
```

## `image_url`

This part is telling the location of the image. There is no way to customize it for now during generating the 
image.

## `attributes`

Attributes is a list of images that are combined into an image. Attributes only has two fields, `trait_type`
and `value`.

### `trait_type`

Trait type here is the folder name in the **layers** directory. Say if it combines image from 
**Layers/Head** directory then in the metadata there will be attribute with a `trait_type` which its value is `Head`.

### `value`

Value is the name of image that is combined. Consider the following example

```
...
    {
      "trait_type": "Eyes",
      "value": "Dark"
    }
...
```

We can see that it has trait type `Eyes`, it means it combines image from **layers/Eyes** directory. Which image 
is used? The answer is on the `value` part. It says `Dark` which means **Dark.png** image is combined into this 
image. Checkout diagram below to understand more

```{figure} ../_static/attributes-mapping.png
Attributes naming
```
