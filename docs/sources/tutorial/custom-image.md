# Create Your Own Image

This section will guide us how to create image so it's recognized by {{project}} and we can combine them using
it. There are some requirements that need to be fulfilled:

1. The name must have `.png` suffix
2. The image is in PNG format
3. Use transparent background for each image except for one that will be used as background
4. Use the same resolution for all images
5. Group image in a folder

```{note}
This section requires you to get through [](getting-started) first
```

## Drawing

Use your preferred drawing tool, in this section I'm using [Inkscape](https://inkscape.org/) for demo purpose.

> I'm not an artist nor affiliated with Inkscape.

Let's draw something simple, a square.

![A blue square has been drawn](../_static/inkscape-square.png)

Let's say we want this to be the first layer (bottom layer) of the image. We can save the image as PNG and
name it as **Blue.png**.

![An image file named Blue.png exist in a directory](../_static/saved-blue-image.png)

We already have one image, let's create 1 more. This time, a gray square and save it as **Gray.png**.

![Two image files exist, they are named Blue.png and Gray.png](../_static/saved-gray-image.png)

## Saving

Our first layer image is ready, next is we have to group them in a folder. Let's create one and name it as 
**Head**. After that, move the images into that folder.

![There is 1 folder named Head and 2 files named Blue.png and Gray.png, the user then move all images into Head folder](../_static/moving-image.gif)

## More Layers

One layer is not enough since we are combining image. So let's create some images for another layer.

![There is 1 folder and 2 image files named Canine.png and Dizzy.png](../_static/more-layer-image.png)

We are going to put **Canine.png** into **Mouth** folder and **Dizzy.png** into **Eyes** folder.

![There are 3 folders named Head, Mouth, and Eyes](../_static/more-layer-folder.png)

## Configuration

The next step is we have to move all folders into **layers** directory which should be at the same location
with {{project}}.

![There are 2 file manager opened, one have directory named layers and other have 3 folders which are Head, Mouth, and Eyes. The user move all folders to layers directory.](../_static/moving-folder-to-layer.gif)

Now we need to update **config.json** to include the new folder name and also empty the distribution. For demo
purpose, we will only generate 2 images.

{emphasize-lines="3,11-13,15"}
```json
{
  "name": "Gambar",
  "amount": 2,
  "batchSize": 4,
  "startTokenId": 1,
  "width": 1000,
  "height": 1000,
  "outputDirectory": "output",
  "unique": false,
  "layers": [
    "Head",
    "Eyes",
    "Mouth"
  ],
  "distribution": {}
}
```

Now, let's execute the tool!

- **Windows**: Double click **{{project_plain}}.exe**
- **Linux/Mac**: In the terminal go to **{{project_plain}}** directory then type `./pic` and press Enter

As always, {{project}} will update the distribution in the **config.json** if it's empty. Check our 
configuration now.

{emphasize-lines="15-26"}
```json
{
  "name": "Gambar",
  "amount": 2,
  "batchSize": 4,
  "startTokenId": 1,
  "width": 1000,
  "height": 1000,
  "outputDirectory": "output",
  "unique": false,
  "layers": [
    "Head",
    "Eyes",
    "Mouth"
  ],
  "distribution": {
    "Eyes": {
      "layers/Eyes/Dizzy.png": 0
    },
    "Head": {
      "layers/Head/Blue.png": 0,
      "layers/Head/Gray.png": 0
    },
    "Mouth": {
      "layers/Mouth/Canine.png": 0
    }
  }
}
```

Our image has been included in the configuration automatically, that means {{project}} will use them to create 
combination. You can continue to tweak the number and execute the tool. Have fun!
