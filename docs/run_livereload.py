from livereload import Server, shell
from os import path

if __name__ == '__main__':
    server = Server()
    directory = "sources"
    server.watch(path.join(directory, '*.rst'), shell('make html'), delay=1)
    server.watch(path.join(directory, '*/**.md'), shell('make html'), delay=1)
    server.watch(path.join(directory, '*.py'), shell('make html'), delay=1)
    server.watch(path.join(directory, '_static/*'), shell('make html'), delay=1)
    server.watch(path.join(directory, '_templates/*'), shell('make html'), delay=1)
    server.serve(root=path.join('_build', 'html'))
