namespace ImageGeneratorTest

open System.IO
open System.Text.Json;

open Xunit
open FsUnit.Xunit

open ImageCombinator.Util
open ImageCombinator.Metadata

module ``Test Util Module`` =
  [<Fact>]
  let ``Match existing file extension`` () =
    let filename = "../../../test-file/config.json"
    let extension = ".json"
    matchExtension(filename, extension) |> should equal true

  [<Fact>]
  let ``Match non-exist file extension`` () =
    let filename = "non-exist-file.json"
    let extension = ".json"
    matchExtension(filename, extension) |> should equal false

  [<Fact>]
  let ``Collect images path`` () =
    let imageDirectory = "../../../../test-file/Images/Bitmap"
    let expected: Option<Map<string, string[]>> =
      [("Bitmap", [|
        "layers/../../../../test-file/Images/Bitmap/bitmap (another copy).png"
        "layers/../../../../test-file/Images/Bitmap/bitmap (copy).png"
        "layers/../../../../test-file/Images/Bitmap/bitmap.png"
      |])]
      |> Map.ofList
      |> Some

    [imageDirectory] |> collectImages |> should equal expected

  [<Fact>]
  let ``Collect file with PNG format only`` () =
    let imageDirectory = "../../../../test-file/Images/MixedFile"
    let expected: Option<Map<string, string[]>> =
      [("MixedFile", [|
        "layers/../../../../test-file/Images/MixedFile/bitmap (copy).png"
        "layers/../../../../test-file/Images/MixedFile/bitmap.png"
      |])]
      |> Map.ofList
      |> Some

    [imageDirectory] |> collectImages |> should equal expected

  [<Fact>]
  let ``Convert JSON record to string`` () =
    let record: Metadata = {
      name = "Gambar #2"
      description = Some "Sebuah deskripsi"
      image_url = "path/to/image.png"
      attributes = Some [
        { trait_type = "Character"; value = "Human" }
      ]
      external_url = Some "http://localhost"
    }

    let result = toJsonString(record) |> JsonSerializer.Deserialize

    result.name |> should equal record.name
    result.description |> should equal record.description
    result.image_url |> should equal record.image_url
    result.attributes |> should equal record.attributes
    result.external_url |> should equal record.external_url

  [<Fact>]
  let ``Load checkpoint file without excluded token ID`` () =
    let checkpointFile = "../../../test-file/Gambar-traits-checkpoint.csv"
    let result: List<Option<string seq>> = [] |> loadCheckpointFile checkpointFile
    let expected: List<Option<string seq>> = [
      Some
        (seq
            ["layers/background/background.png"; "layers/body/body.png";
            "layers/eyes/potatoes-7.png"])
      Some
        (seq
            ["layers/background/background.png"; "layers/body/body.png";
            "layers/eyes/potatoes-6.png"])
      Some
        (seq
            ["layers/background/background.png"; "layers/body/body.png";
            "layers/eyes/potatoes-10.png"])
      Some
        (seq
            ["layers/background/background.png"; "layers/body/body.png";
            "layers/eyes/potatoes-8.png"])
    ]

    let compareResult (index: int) (value: Option<string seq>) =
      let output = value |> Option.get |> List.ofSeq
      let expectedOutput = Option.get expected[index]
      output |> should equal expectedOutput

    result |> List.iteri compareResult

  [<Fact>]
  let ``Load checkpoint file with excluded token ID`` () =
    let checkpointFile = "../../../test-file/Gambar-traits-checkpoint.csv"
    let result: List<Option<string seq>> = [1u;3u] |> loadCheckpointFile checkpointFile
    let expected: List<Option<string seq>> = [
      Some
        (seq
            ["layers/background/background.png"; "layers/body/body.png";
            "layers/eyes/potatoes-6.png"])
      Some
        (seq
            ["layers/background/background.png"; "layers/body/body.png";
            "layers/eyes/potatoes-8.png"])
    ]

    let compareResult (index: int) (value: Option<string seq>) =
      let output = value |> Option.get |> List.ofSeq
      let expectedOutput = Option.get expected[index]
      output |> should equal expectedOutput

    result |> List.iteri compareResult

  [<Fact>]
  let ``Evaluate answer with YES`` () =
    "YES" |> evaluateAnswer |> should equal true
    "Y" |> evaluateAnswer |> should equal true

  [<Fact>]
  let ``Evaluate answer with other than YES`` () =
    "No" |> evaluateAnswer |> should equal false
    "N" |> evaluateAnswer |> should equal false
    "Mbo" |> evaluateAnswer |> should equal false

  [<Fact>]
  let ``Should calculate distribution`` () =
    let amount = 12u
    let distribution =
      Some (Map [("Skin",
              Map [
                ("Cyborg", 30.0)
                ("Human", 50.0)
                ("Reptile", 20.0)
          ])])

    let result =
      amount
      |> calculateDistribution distribution
      |> Option.get

    let expectedDistribution: List<uint> = [4u;6u;3u]

    let compareDistribution (index: int) (value: ImageCombinator.AssetDistribution) =
      value.Quota |> should equal expectedDistribution[index]

    result["Skin"]
    |> Map.values
    |> Seq.iteri compareDistribution
