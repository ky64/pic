open System
open Xunit
open FsUnit.Xunit

open ImageCombinator.Metadata

module ``Test Metadata Module`` =
  module ``createAttributes function`` =
    [<Fact>]
    let ``Create attribute`` () =
      let traits = seq [
        "Body/Cyborg.png"
        "Costume/DemiGod.png"
        "Eyes/Devil.png"
        "Mouth/Voodoo.png"
      ]

      let expected: Option<Attributes list> = Some [
        { trait_type = "Body"; value = "Cyborg" }
        { trait_type = "Costume"; value = "DemiGod" }
        { trait_type = "Eyes"; value = "Devil" }
        { trait_type = "Mouth"; value = "Voodoo" }
      ]

      Some traits |> createAttributes |> should equal expected

    [<Fact>]
    let ``Create attribute with unordered input should give ordered input`` () =
      let traits = seq [
        "Eyes/Devil.png"
        "Mouth/Voodoo.png"
        "Body/Cyborg.png"
        "Costume/DemiGod.png"
      ]

      let expected: Option<Attributes list> = Some [
        { trait_type = "Body"; value = "Cyborg" }
        { trait_type = "Costume"; value = "DemiGod" }
        { trait_type = "Eyes"; value = "Devil" }
        { trait_type = "Mouth"; value = "Voodoo" }
      ]

      Some traits |> createAttributes |> should equal expected

    [<Fact>]
    let ``Create attribute with mixed filepath input`` () =
      let traits = seq [
        "Eyes/Devil.png"
        "Voodoo.png"
        "Cyborg.png"
        "Costume/demi/Demi God.png"
      ]

      let expected: Option<Attributes list> = Some [
        { trait_type = ""; value = "Cyborg" }
        { trait_type = ""; value = "Voodoo" }
        { trait_type = "Eyes"; value = "Devil" }
        { trait_type = "demi"; value = "Demi God" }
      ]

      Some traits |> createAttributes |> should equal expected

    [<Fact>]
    let ``Create attribute with empty sequence input`` () =
      let traits = seq []
      let expected: Option<Attributes list> = Some []
      Some traits |> createAttributes |> should equal expected

    [<Fact>]
    let ``Create attribute with None input`` () =
      let traits = None
      let expected: Option<Attributes list> = None
      traits |> createAttributes |> should equal expected

  module ``createMetadata function`` =

    [<Fact>]
    let ``Create metadata`` () =
      let tokenId = 12
      let name = "Gambar"
      let description = "Sebuah deskripsi mengenai gambar" |> Some
      let traits =
        seq [
        "Body/Cyborg.png"
        "Costume/DemiGod.png"
        "Eyes/Devil.png"
        "Mouth/Voodoo.png"
        ] |> Some
      let external_url = "http://localhost" |> Some
      let image_url = $"path/to/image/{tokenId}.png"
      let metadata = createMetadata(name, tokenId, description, image_url, traits, external_url)
      let expected: Metadata = {
        name = "Gambar #12"
        description = Some "Sebuah deskripsi mengenai gambar"
        image_url = image_url
        attributes = Some [
          { trait_type = "Body"; value = "Cyborg" }
          { trait_type = "Costume"; value = "DemiGod" }
          { trait_type = "Eyes"; value = "Devil" }
          { trait_type = "Mouth"; value = "Voodoo" }
        ]
        external_url = Some "http://localhost"
      }

      metadata |> should equal expected

    [<Fact>]
    let ``Create metadata with empty description and external URL`` () =
      let tokenId = 0
      let name = "Gambar"
      let traits =
        seq [
        "Body/Cyborg.png"
        "Costume/DemiGod.png"
        "Eyes/Devil.png"
        "Mouth/Voodoo.png"
        ] |> Some
      let image_url = $"path/to/image/{tokenId}.png"
      let metadata = createMetadata(name, tokenId, None, image_url, traits, None)
      let expected: Metadata = {
        name = "Gambar #0"
        description = None
        image_url = image_url
        attributes = Some [
          { trait_type = "Body"; value = "Cyborg" }
          { trait_type = "Costume"; value = "DemiGod" }
          { trait_type = "Eyes"; value = "Devil" }
          { trait_type = "Mouth"; value = "Voodoo" }
        ]
        external_url = None
      }

      metadata |> should equal expected

